# 1. FizzBuzz : how to run
In root folder project : 
`docker run -it --rm --name fizzBuzz -v "$PWD":/usr/src/myapp -w /usr/src/myapp php:8-cli php index.php`

# 2. JS
In root project folder, launch 'testjs.html' in your favourite browser

# 3. Data parser (PHP)
` docker run -it --rm --name data-parser -v "$PWD":/usr/src/myapp -w /usr/src/myapp php:8-cli php parser.php`

