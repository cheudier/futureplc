<?php
require_once 'RowParser.php';

class Parser {
    public $appCodes = [];

    static $headers = [
        'id',
        'appCode',
        'deviceId',
        'contactable',
        'subscription_status',
        'has_downloaded_free_product_status',
        'has_downloaded_iap_product_status',
    ];

    /**
     * @var int autoincr used
     */
    private $id = 0;

    public function __construct(
        public string $rootPath,
        public string $iniFile
    )
    {
        $this->appCodes =array_flip($this->getAppCodes($rootPath.'/'.$iniFile));
    }

    public function processAllFiles() {
        $files = $this->getDirContents($this->rootPath);

        foreach($files as $file) {
            $this->logFileToCsv($file);
        }
    }

    /**
     * get all files not in the first dir
     *
     * @param $path
     * @return array
     */
    protected function getDirContents($path) {
        $rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));

        $files = array();
        foreach ($rii as $file) {
            if($rii->getDepth() === 0) {
                continue;
            }
            if (!$file->isDir()) {
                $files[] = $file->getPathname();
            }
        }

        return $files;
    }

    /**
     * TODO : do not write line by line in files,
     */
    public function logFileToCsv($file) {
        $lines = file('parser_test/20131004/device-tokens-for-sfx-collection-1.log', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $fp = fopen('parser_test/20131004/device-tokens-for-sfx-collection-1.csv', 'w');

        // remove header from original array
        array_shift($lines);

        // add headers
        fputcsv($fp, self::$headers);

        foreach ($lines as $key => $file) {
            $row = explode(',',$file);
            $formatedRow = $this->formatRow($row);
            fputcsv($fp, $formatedRow);
        }
        fclose($fp);
    }

    protected function formatRow($row): array {
        $rowParser = new RowParser($row, $this->appCodes);
        $formatedRow = $rowParser->formatRow();
        // add autoincr id
        array_unshift($formatedRow,$this->id++);

        return $formatedRow;
    }

    public function getAppCodes(string $path = 'parser_test/appCodes.ini'):array {
        try{
            $iniData = parse_ini_file($path, true);
            if(isset($iniData['appcodes'])) {
                return $iniData['appcodes'];
            } else{
                throw new Exception('No section appcodes found in path : '.$path);
            }
        } catch (Exception $e) {
            error_log($e);
            return [];
        }
    }
}