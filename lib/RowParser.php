<?php


class RowParser
{
    static private $valid_subscription_status = [
        'active_subscriber',
        'expired_subscriber',
        'never_subscribed',
        'subscription_unknown',
    ];

    static private $valid_has_downloaded_free_product_status = [
        'has_downloaded_free_product',
        'not_downloaded_free_product',
        'downloaded_free_product_unknown',
    ];

    static private $valid_has_downloaded_iap_product_status = [
        'has_downloaded_iap_product',
        'not_downloaded_free_product',
        'downloaded_iap_product_unknown',
    ];

    private $appCodesIni = '';
    private $appCode = '';
    private $token = '';
    private $status = '';
    private $tags = [];

    private $errorTags = '';

    public function __construct(array $rowData, array $appCodes)
    {
        $this->appCodesIni = $appCodes;
        $this->appCode = $rowData[0] ?? '';
        $this->token = $rowData[1] ?? '';
        $this->status = $rowData[2] ?? '';
        $this->tags = isset($rowData[3]) ? explode('|', $rowData[3]) : [];
    }

    public function formatRow() {
        $output = [];
        $output[0] = $this->getAppCode();
        $output[1] = $this->token;
        $output[2] = $this->getContactable();
        $output[3] = $this->getValidTag(self::$valid_subscription_status);
        $output[4] = $this->getValidTag(self::$valid_has_downloaded_free_product_status);
        $output[5] = $this->getValidTag(self::$valid_has_downloaded_iap_product_status);

        return $output;
    }

    public function getAppCode() {
        return isset($this->appCodesIni[$this->appCode]) ? $this->appCodesIni[$this->appCode] : 'unkown';
    }

    public function getContactable () {
        return $this->status == 1 ? '1' : '0';
    }

    public function getValidTag(array $validTags) {
        $output = '';
        foreach($this->tags as $key => $tag) {
            if(in_array($tag, $validTags)) {
                if(!empty($output)) {
                    $this->errorTags .= $tag;
                    // todo : check foreach still correct
                    array_splice($this->tags, $key, $key);
                } else {
                    $output = $tag;
                }
            }
        }
        return $output;
    }
}