const http = require('http');

// https.get('https://search-api.fie.future.net.uk/widget.php?id=review&site=TRD&model_name=iPad_Air', (resp) => {
//     let data = '';
//
//     // A chunk of data has been received.
//     // resp.on('data', (chunk) => {
//     //     data += chunk;
//     // });
//
//
//     // The whole response has been received. Print out the result.
//     resp.on('end', () => {
//         console.log(data);
//         console.log(JSON.parse(data));
//     });
//
// }).on("error", (err) => {
//     console.log("Error: " + err.message);
// });

http.get('http://search-api.fie.future.net.uk/widget.php?id=review&site=TRD&model_name=iPad_Air', (res) => {

    const { statusCode } = res;
    const contentType = res.headers['content-type'];

    let error;
    // Any 2xx status code signals a successful response but
    // here we're only checking for 200.
    if (statusCode !== 200) {
        error = new Error('Request Failed.\n' +
            `Status Code: ${statusCode}`);
    } else if (!/^application\/json/.test(contentType)) {
        error = new Error('Invalid content-type.\n' +
            `Expected application/json but received ${contentType}`);
    }
    if (error) {
        console.error(error.message);
        // Consume response data to free up memory
        res.resume();
        return;
    }


    res.setEncoding('utf8');
    let rawData = '';
    res.on('data', (chunk) => { rawData += chunk; });
    res.on('end', () => {
        try {
            const parsedData = JSON.parse(rawData);
            console.log(parsedData);
        } catch (e) {
            console.error(e.message);
        }
    });
}).on('error', (e) => {
    console.error(`Got error: ${e.message}`);
});

    // console.log('statusCode:', res.statusCode);
    // console.log('headers:', res.headers);
    //
    // res.on('data', (d) => {
    //     // console.log(d);
    //     process.stdout.write(d);
    // });

// }).on('error', (e) => {
//     console.error(e);
// });