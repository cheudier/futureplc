<?php

/**
 * @param integer $number
 * @return bool
 */
function primeCheck($number) {
    if ($number == 1) {
        return false;
    }
    for ($i = 2; $i <= $number/2; $i++){
        if ($number % $i == 0)
            return false;
    }
    return true;
}


function getFizzBuzz() {
    $output = '';

    for($i=1; $i<=500; $i++){
        $row = '';
        // is number prime ?
        if(primeCheck($i)) {
            $row .= 'FiZZBUZZ++';
        } else {
            // if not a prime, check if divisble by 3
            if($i % 3 === 0) {
                $row .= 'FIZZ';
            }
            // if not a prime, check if divisble by 5
            if($i % 5 === 0) {
                $row .= 'BUZZ';
            }
        }
        // if not a prime nor divisible by 3 nor 5, output $i
        if(empty($row)) {
            $row = $i;
        }
        $row .= PHP_EOL;

        $output .= $row;
    }
    return $output;
}

$output = getFizzBuzz();
echo $output;
file_put_contents('fizzbuzz.log', $output,  FILE_APPEND);
