<?php

require_once 'lib/Parser.php';

// Start the clock time in seconds
$start_time = microtime(true);

$parser = new Parser('parser_test', 'appCodes.ini');
$parser->processAllFiles();

// End the clock time in seconds
$end_time = microtime(true);
// Calculate the script execution time
$execution_time = ($end_time - $start_time);
echo 'Executed in ' . $execution_time . 's' . PHP_EOL;